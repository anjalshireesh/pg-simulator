require 'zlib'

class PaymentController < ApplicationController
  CHECKSUM_KEY = "PDdMbZMgf97E"

  def index
  end

  def create
    parse_request
    prepare_success_msg
    prepare_failure_msg
    prepare_invalid_msg
    render 'payment'
  end

  def prepare_success_msg
    txn_ref_num = generate_ref_num
    bank_ref_num = generate_ref_num
# TODO: randomize bank_id
    bank_id = "SBI"
    bank_merchant_id = generate_ref_num
    txn_type = "NA"
    item_code = "NA"
    security_type = "NA"
    security_password = "NA"
    txn_date = Time.new.strftime '%d-%m-%Y %H:%M:%S'
    auth_status = "0300"
    settlement_type = "NA"
    error_status = "NA"
    error_description = "NA"

    msg = "#{@merchant_id}|#{@customer_id}|#{txn_ref_num}|#{bank_ref_num}|#{@amount}|#{bank_id}|#{bank_merchant_id}|#{txn_type}|#{@currency}|#{item_code}|#{security_type}|#{@security_id}|#{security_password}|#{txn_date}|#{auth_status}|#{settlement_type}|#{@additional_info_1}|#{@additional_info_2}|#{@additional_info_3}|#{@additional_info_4}|#{@additional_info_5}|#{@additional_info_6}|#{@additional_info_7}|#{error_status}|#{error_description}"

    checksum = calculate_checksum("#{msg}|#{CHECKSUM_KEY}")
    @success_msg = "#{msg}|#{checksum}"
  end

  def prepare_failure_msg
    txn_ref_num = generate_ref_num
    bank_ref_num = generate_ref_num
# TODO: randomize bank_id
    bank_id = "SBI"
    bank_merchant_id = generate_ref_num
    txn_type = "NA"
    item_code = "NA"
    security_type = "NA"
    security_password = "NA"
    txn_date = Time.new.strftime '%d-%m-%Y %H:%M:%S'
    auth_status = "0400"
    settlement_type = "NA"
    error_status = "AUTH_FAILURE"
    error_description = "Authentication Failed!"

    msg = "#{@merchant_id}|#{@customer_id}|#{txn_ref_num}|#{bank_ref_num}|#{@amount}|#{bank_id}|#{bank_merchant_id}|#{txn_type}|#{@currency}|#{item_code}|#{security_type}|#{@security_id}|#{security_password}|#{txn_date}|#{auth_status}|#{settlement_type}|#{@additional_info_1}|#{@additional_info_2}|#{@additional_info_3}|#{@additional_info_4}|#{@additional_info_5}|#{@additional_info_6}|#{@additional_info_7}|#{error_status}|#{error_description}"

    checksum = calculate_checksum("#{msg}|#{CHECKSUM_KEY}")
    @failure_msg = "#{msg}|#{checksum}"
  end

  def prepare_invalid_msg
    txn_ref_num = "NA"
    bank_ref_num = "NA"
    bank_id = "NA"
    bank_merchant_id = "NA"
    txn_type = "NA"
    item_code = "NA"
    security_type = "NA"
    security_password = "NA"
    txn_date = "NA"
    auth_status = "0002"
    settlement_type = "NA"
    error_status = "ERR064"
    error_description = "Sorry! Your request could not be processed. This could be due to incorrect values input for authorisation. Please try again."

    msg = "#{@merchant_id}|NA|#{txn_ref_num}|#{bank_ref_num}|#{@amount}|#{bank_id}|#{bank_merchant_id}|#{txn_type}|NA|#{item_code}|#{security_type}|#{@security_id}|#{security_password}|#{txn_date}|#{auth_status}|#{settlement_type}|NA|NA|NA|NA|NA|NA|NA|#{error_status}|#{error_description}"

    checksum = calculate_checksum("#{msg}|#{CHECKSUM_KEY}")
    @invalid_msg = "#{msg}|#{checksum}"
  end

  def parse_request
    @payment_request = request.POST[:msg]
    request_elements = @payment_request.split("|")
    @merchant_id = request_elements[0]
    @customer_id = request_elements[1]
    @amount = request_elements[3]
    @currency = request_elements[7]
    @type_field_1 = request_elements[9]
    @security_id = request_elements[10]
    @type_field_2 = request_elements[13]
    @additional_info_1 = request_elements[14]
    @additional_info_2 = request_elements[15]
    @additional_info_3 = request_elements[16]
    @additional_info_4 = request_elements[17]
    @additional_info_5 = request_elements[18]
    @additional_info_6 = request_elements[19]
    @additional_info_7 = request_elements[20]
    @return_url = request_elements[21]
    @checksum = request_elements[22]
  end

  def generate_ref_num
    rand(36**8).to_s(36)
  end

  def calculate_checksum(str)
    Zlib::crc32(str)
  end
end

